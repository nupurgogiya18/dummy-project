﻿namespace VMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    public partial class CKEditor
    {
        public CKEditor()
        {

        }
        [Required]
        [DisplayName("PO Value")]
        public string serviceValue { get; set; }

        [Required]
        [DisplayName("PO Name")]
        public string poReportName { get; set; }

        [Required]
        [DisplayName("PO Value")]

        public string poReportValue { get; set; }

        [Required]
        [DisplayName("PO Number")]
        public string poNumber { get; set; }

        [Required]
        [DisplayName("Category Name")]
        public string categoryName { get; set; }

        [Required]
        [DisplayName("Department Name")]

        public string userDepartmentName { get; set; }

        public string serviceCode { get; set; }
        public string vendorId { get; set; }

        [Required]
        [DisplayName("Vendor Name")]
        public string vendorName { get; set; }


        [DisplayName("Status")]
        public string poReportStatus { get; set; }

        [Required]
        [DisplayName("PO Reason")]
        [MaxLength(1000)]
        public string poReportReason { get; set; }

        [Required]
        [DisplayName("POC")]
        public string poc { get; set; }




        [Required]
        [DisplayName("User Name")]
        public string employeeName { get; set; }

        [Required]
        [DisplayName("First Level Approver")]
        public string firstLevelApprover { get; set; }

        [Required]
        [DisplayName("Vendor Email")]
        public string vendoremail { get; set; }

       
        [DisplayName("PO ReportId")]
        public string poReportId { get; set; }




        [Required]
        public string Subject { get; set; }


        // public string QuotationDetails { get; set; }

        [Required]
        public string Details { get; set; }

        [Required]
        public string deliverytextbox { get; set; }

        public int count { get; set; }

        [DisplayName("PO Date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime PoDate { get; set; }

        [DisplayName("Contract From Date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime ContractFromDate { get; set; }

        [DisplayName("Contract to Date")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime ContractToDate { get; set; }

        [DisplayName("Quotation Number")]
        [Required]
        public string QuotationNumber { get; set; }

        [DisplayName("Quotation Date")]
        [DataType(DataType.Date)]
        [Required]
        
        public DateTime QuotationDate { get; set; }

        [Required]
        [DisplayName("Billing Adrdress(HSPL)")]
        public int hsplBillingAddressId { get; set; }

        [DisplayName("Billing Adrdress(HILPL)")]
        public int hilplBillingAddressId { get; set; }

        [DisplayName("Billing Adrdress(HTV)")]
        public int htvBillingAddressId { get; set; }

        [DisplayName("Billing Adrdress(HKP)")]
        public int hkpplBillingAddressId { get; set; }

        
        public string[] BillingUnit { get; set; }

        [DisplayName("Shipping Address(HSPL)")]
        public int hsplShippingAddressId { get; set; }

        [DisplayName("Shipping Address(HILPL)")]
        public int hilplShippingAddressId { get; set; }

        [DisplayName("Shipping Address(HTV)")]
        public int htvShippingAddressId { get; set; }

        [DisplayName("Shipping Address(HKP)")]
        public int hkpplShippingAddressId { get; set; }

        [Required]
        public int PaymentTermsId { get; set; }

        [Required]
        public string PaymentTerms { get; set; }

        [Required]
        public int TaxesId { get; set; }

        [Required]
        public string TaxesTerms { get; set; }

        [DisplayName("Payment Terms")]
        [Required]
        public string[] paymentDetail { get; set; }

        [DisplayName("Taxes Terms")]
        [Required]
        public string[] taxDetail { get; set; }
        public string HILprice { get; set; }
        public string HSPLprice { get; set; }
        public string HTVprice { get; set; }
        public string HKPprice { get; set; }
        [Required]
        public string HILpercent { get; set; }

        [Required]
        public string HSPLpercent { get; set; }

        [Required]
        public string HTVpercent { get; set; }

        [Required]
        public string HKPpercent { get; set; }

        public int addressId { get; set; }
        public string locationName { get; set; }
        public string locationAddress { get; set; }
    }
}
