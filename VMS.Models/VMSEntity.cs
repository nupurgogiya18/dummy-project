﻿
namespace VMS.Models
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public partial class VMSEntity
    {

        public VMSEntity()
        {

        }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string userEmail { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "Password")]
        public string userPassword { get; set; }


        public string designation { get; set; }

        [Required]
        [Display(Name = "Employee Id")]
        public string employeeId { get; set; }

        [Display(Name = "User Name")]
        public string userName { get; set; }


        [Required]
        [Display(Name = "Vendor Name")]
        public string vendorId { get; set; }


        [Required]
        [Display(Name = "Vendor Name")]
        public string vendorName { get; set; }


        [Required]
        [Display(Name = "Project Type")]
        public string typeName { get; set; }


        [Required]
        [Display(Name = "Category Name")]
        public int serviceCategoryId { get; set; }

        [Required]
        [Display(Name = "Category Name")]
        public string categoryName { get; set; }

        [Required]
        [Display(Name = "User Department")]
        public int userDepartmentId { get; set; }


        [Required]
        [Display(Name = "User Department")]
        public string userDepartmentName { get; set; }




        [Required]
        [Display(Name = "Service Name")]
        public int serviceTypeId { get; set; }



        [Required]
        [Display(Name = "Service Name")]
        public string serviceName { get; set; }

        [Required]
        [Display(Name = "First Level Aprrover")]
        public string firstLevelApprover { get; set; }


        [Required]
        [Display(Name = "Second Level Approver")]
        public string secondLevelApprover { get; set; }


        [Required]
        [Display(Name = "Third Level Approver")]
        public string thirdLevelApprover { get; set; }



        [Required]
        [Display(Name = "Billing Unit")]
        public string billingUnit { get; set; }


        [Required]
        [Display(Name = "Details")]
        public string details { get; set; }


        [Required]
        [Display(Name = "Status")]
        public string serviceCode { get; set; }


        [Required]
        [Display(Name = "Status")]
        public string serviceStatus { get; set; }


        [Required]
        [Display(Name = "PO Number")]
        public string poNumber { get; set; }


        [DataType(DataType.Date)]
        [Required]
        [Display(Name = "Start Date")]
        public DateTime? startDate { get; set; }

        [DataType(DataType.Date)]
        [Required]
        [Display(Name = "End Date")]
        public DateTime? endDate { get; set; }


        [Display(Name = "Service Value")]
        public string serviceValue { get; set; }


        [Display(Name = "PO Id")]
        public string poReportId { get; set; }

        [Display(Name = "PO Value")]
        public string poReportValue { get; set; }

        [Display(Name = "PO Status")]
        public string poReportStatus { get; set; }

       
        public string approver1 { get; set; }


        public string approver2 { get; set; }
        public string approver3 { get; set; }

        [Display(Name = "PO Name")]
        public string poReportName { get; set; }

       
        public string sprintId { get; set; }
        public string sprintName { get; set; }
        public DateTime? sprintEndDate { get; set; }

        [Display(Name = "Percent Service Value")]
        public string percentOfProjectValue { get; set; }
        public string sprintValue { get; set; }
        public int PaymentTermsId { get; set; }
        public string PaymentTerms { get; set; }
        public string[] Payment_Terms { get; set; }
        public int TaxesId { get; set; }
        public string TaxesTerms { get; set; }
        public float HILprice { get; set; }
        public float HSPLprice { get; set; }
        public float HTVprice { get; set; }
        public float HKVprice { get; set; }
        public string[] tempService { get; set; }
        public int addressId { get; set; }
        public string locationName { get; set; }
        public string locationAddress { get; set; }
    }
}
