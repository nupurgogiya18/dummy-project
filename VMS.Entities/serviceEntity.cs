﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.Entities
{
    public partial class serviceEntity
    {

        public serviceEntity()
        {


        }

        [Required]
        [EmailAddress]
        public string userEmail { get; set; }

        [DataType(DataType.Password)]
        [Required]

        public string userPassword { get; set; }



        public string employeeId { get; set; }
        public string userName { get; set; }
        public string vendorId { get; set; }

        public string vendorName { get; set; }

        public string typeName { get; set; }

        public int serviceTypeId { get; set; }
    }

}
