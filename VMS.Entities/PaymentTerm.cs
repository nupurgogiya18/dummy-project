//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VMS.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentTerm
    {
        public int PaymentTermsId { get; set; }
        public string PaymentTerms { get; set; }
    }
}
