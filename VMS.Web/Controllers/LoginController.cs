﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using VMS.Web.Controllers.Security;
//using VMSHTV.Models;
using System.Web.Security;
using VMS.Models;
using VMS.BLL;
using VMS.Entities;
using VMS.DAL.Infrastructure;
using System.Configuration;
using System.Net.Mail;
using System.IO;
using VMS.Web.InternalBL;

namespace VMS.Web.Controllers 
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
       
        // protected UserLoginBL _UserLogin;
        [HttpGet]
        public ActionResult Login()
        { 
           // _UserLogin = new UserLoginBL();
            return View();
        }


        [HttpPost]
        public ActionResult Login(User _userTableDto)
        {


            try
            {
                ServiceVMSEntities context = new ServiceVMSEntities();
                var context1 = HttpContext.Response.Cookies;
                var user = unitOfWork.Login(_userTableDto.userEmail, _userTableDto.userPassword);
                var valid_user = context.Users.Where(x => x.userEmail == _userTableDto.userEmail && x.userPassword == _userTableDto.userPassword).Select(x => x);

                User login = valid_user.FirstOrDefault();
                Session["Email"] = login.userName;

                Session["employeeId"] = login.employeeId;


                var chkTimeOut = Session.Timeout;
                var cookie_user = InternalBL.Implementations.CookieHandler.SetUserCookie(context1, _userTableDto);
                
                if (ModelState.IsValid && user == true)
                {
                    FormsAuthentication.SetAuthCookie(_userTableDto.userEmail, false);
                    
                    return RedirectToAction("Index", "Dashboard");
                }

                else
                {
                    TempData["Msg"] = "Login Failed";
                    return View();
                }

              

            }
            catch (Exception E1)
            {
                TempData["Msg"] = "Login Failed  " + E1.Message;
                Console.WriteLine(E1);
                return RedirectToAction("Index");
            }
        }

        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login");
        }

        public string ForgotPassword(VMSEntity model)
        {
            string empid = string.Empty;
            string id = model.userEmail;
            int count = 0;
            try
            {

                using (var writer = new StreamWriter(@"C:\VMSDocs\logfile.txt"))
                {

                    MailMessage mail = new MailMessage();
                    string username = ConfigurationManager.AppSettings["SmtpUserId"].ToString();
                    mail.To.Add(id);
                    mail.From = new MailAddress(username);
                    mail.Subject = "VMS Password Reset";

                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                    writer.WriteLine(chars);
                    var stringChars = new char[8];
                    var random = new Random();

                    for (int i = 0; i < stringChars.Length; i++)
                    {
                        stringChars[i] = chars[random.Next(chars.Length)];
                    }

                    var finalString = new String(stringChars);

                    string Body = finalString;
                    string Data = "Hi," +
                        "<br>" +
                                    "A request has been made to reset your VMS account password. " + "<br><br>" + "Please find your new password below." +
                                 "<br><br>" +
                                    "Password : " + "<b>" + Body + "</b>" +
                                    "<br><br>" +
                                     "Do not share this password with anyone." +
                                     "<br><br>" +
                                    "You can change your password as per your convenience by selecting the ‘’Change Password’’ option.";
                    mail.Body = Data;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = ConfigurationManager.AppSettings["SmtpServerHost"].ToString();
                    smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpServerPort"]);
                    smtp.UseDefaultCredentials = false;

                    string password = ConfigurationManager.AppSettings["SmtpUserPassword"].ToString();

                    smtp.Credentials = new System.Net.NetworkCredential(username, password);
                    smtp.Send(mail);
                    count = 1;


                    ServiceVMSEntities context = new ServiceVMSEntities();


                    var vendorid = (from t in context.Vendors
                                    where t.vendorEmail == model.userEmail
                                    select new { t.vendorId });
                    foreach (var q in vendorid)
                    {
                        empid = q.vendorId;
                    }



                    if (empid != string.Empty)
                    {

                        var vendorDetails = (from t in context.Vendors
                                             where t.vendorEmail == model.userEmail
                                             select t).SingleOrDefault();
                        vendorDetails.vendorPassword = finalString;

                        context.SaveChanges();

                    }
                    else
                    {
                        var UserDetails = (from t in context.Users
                                           where t.userEmail == model.userEmail
                                           select t).SingleOrDefault();
                        UserDetails.userPassword = finalString;

                        context.SaveChanges();
                    }



                    // TempData["message"] = "Password sent to your email address";
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            var sprintJson = "{\"count\":" + count + "}";
            return sprintJson;
        }


    }
}