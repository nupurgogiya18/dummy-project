﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VMS.DAL.Infrastructure;
using VMS.Entities;
using VMS.Models;

namespace VMS.Web.Controllers
{
    public class Add_ServiceController : Controller
    {


        private UnitOfWork unitOfWork = new UnitOfWork();
        // GET: Add_Service
        public ActionResult Index()
        {
           
           var vendor = new Vendor();
           var service_category = new ServiceCategory();
           var service_type = new ServiceType();
           var approver1 = new User();
           var userDepartmenttype = new ServiceUserDepartment();
         


            ViewBag.vendorList = unitOfWork.DepartmentRepository.Get(vendor, null);
            ViewBag.serviceCategoryList = unitOfWork.DepartmentRepository.Get(service_category, 0);
            ViewBag.serviceTypeList = unitOfWork.DepartmentRepository.Get(service_type, 0);
            ViewBag.firstLevelApproverList = unitOfWork.DepartmentRepository.Get(approver1, "Approver1",null);
            ViewBag.secondLevelApproverList = unitOfWork.DepartmentRepository.Get(approver1, "Approver2",null);
            ViewBag.thirdLevelApproverList = unitOfWork.DepartmentRepository.Get(approver1, "Approver3", null);
            ViewBag.userDepartment = unitOfWork.DepartmentRepository.Get(userDepartmenttype, 0);
            ViewBag.serviceCode = unitOfWork.DepartmentRepository.auto_generatedCode();
            return View();
        }

        

        [HttpPost]
       
        public ActionResult Index(VMSEntity add_service)
          {
            
            

                var service = Mapper.Map<VMSEntity, Service>(add_service);
                var service_user = Mapper.Map<VMSEntity, ServiceUser>(add_service);
                var service_vendor = Mapper.Map<VMSEntity, ServiceVendor>(add_service);

                unitOfWork.DepartmentRepository.Insert(service);

                unitOfWork.DepartmentRepository.Insert(service_user);

                unitOfWork.DepartmentRepository.Insert(service_vendor);
                unitOfWork.Save();
         
            return RedirectToAction("Create", "Add_POReport", new {service_code = add_service.serviceCode, vendor_Id = add_service.vendorId });
        }

        [HttpGet]
        public JsonResult GetApproverdata(int count)
        {


            // string service_Code = serviceCode;
            var approver1 = new User();

            var firstLevelApproverList = unitOfWork.DepartmentRepository.Get(approver1, "Approver1", count);
            var secondLevelApproverList = unitOfWork.DepartmentRepository.Get(approver1, "Approver2", count);
            var thirdLevelApproverList = unitOfWork.DepartmentRepository.Get(approver1, "Approver2", count);



            //var sprintJson = "{\"FirstLevelApprover\":\"" + firstLevelApproverList + "\",\"SecondLevelApprover\":" + secondLevelApproverList + ",\"ThirdLevelApprover\":" + thirdLevelApproverList + "}";



            return Json(firstLevelApproverList.ToList(), JsonRequestBehavior.AllowGet);
        }


        public ActionResult ServiceView(string sortOrder, string searchString, DateTime? startDate, DateTime? endDate, string Name)
        {


            string display = "0";
            ViewBag.display = display;
            int count = 0;
            string empid = null;
            HttpCookie httpCookie1 = Request.Cookies["employeeId"];
            if (httpCookie1 != null)
            {
                empid = httpCookie1.Value;
            }
            ViewBag.employeeId = empid;
            var vmsentity = new List<VMSEntity>();

            try
            {

                ServiceVMSEntities context = new ServiceVMSEntities();


                count = 1;
                var ProjectDetails = (from t in context.Vendors
                                      join sc in context.Services on t.vendorId equals sc.vendorId
                                      join b in context.ServiceUsers on sc.serviceCode equals b.serviceCode
                                      join l in context.ServiceTypes on sc.serviceTypeId equals l.serviceTypeId
                                      join n in context.ServiceCategories on sc.serviceCategoryId equals n.serviceCategoryId
                                      join o in context.ServiceUserDepartments on sc.userDepartmentId equals o.userDepartmentId
                                     
                                      select new
                                      {
                                          t.vendorId,
                                          t.vendorName,
                                         l.typeName,
                                          sc.poNumber,
                                          sc.startDate,
                                          sc.endDate,
                                          n.categoryName,
                                         o.userDepartmentName,
                                          sc.serviceStatus,
                                          sc.serviceCode,
                                          

                                         

                                          

                                      });

                //ViewBag.CurrentSort = sortOrder;
                //ViewBag.PnameSortParm = sortOrder == "projectname" ? "projectname_desc" : "projectname";
                //ViewBag.startDateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
                //ViewBag.endDateSortParm = sortOrder == "EndDate" ? "Enddate_desc" : "EndDate";
                //ViewBag.vnameSortParm = sortOrder == "vendorname" ? "vendorname_desc" : "vendorname";
                //ViewBag.projectCodeSortParm = sortOrder == "projectCode" ? "projectCode_desc" : "projectCode";
                //ViewBag.dunameSortParm = sortOrder == "duname" ? "duname_desc" : "duname";
                //ViewBag.projectTypeSortParm = sortOrder == "servicename" ? "servicename_desc" : "servicename";
                //ViewBag.clientNameSortParm = sortOrder == "clientName" ? "clientName_desc" : "clientName";

                //if (!String.IsNullOrEmpty(searchString))
                //{

                //    if (Name == "projectName")
                //    {

                //        ProjectDetails = ProjectDetails.Where(s => s.projectName.Contains(searchString));

                //    }
                //    else if (Name == "type")
                //    {
                //        ProjectDetails = ProjectDetails.Where(s => s.type.Contains(searchString));
                //    }
                //    else if (Name == "vendorName")
                //    {
                //        ProjectDetails = ProjectDetails.Where(s => s.vendorName.Contains(searchString));
                //    }
                //    else if (Name == "poNumber")
                //    {
                //        ProjectDetails = ProjectDetails.Where(s => s.poNumber.Contains(searchString));
                //    }

                   

                //    else if (Name == "clientName")
                //    {
                //        ProjectDetails = ProjectDetails.Where(s => s.clientName.Contains(searchString));
                //    }
                //    else if (Name == "orderBookNo")
                //    {
                //        ProjectDetails = ProjectDetails.Where(s => s.orderBookNo.Contains(searchString));
                //    }
                //    else if (Name == "userName")
                //    {
                //        ProjectDetails = ProjectDetails.Where(s => s.userName.Contains(searchString));
                //    }
                //    else if (Name == "duName")
                //    {
                //        ProjectDetails = ProjectDetails.Where(s => s.duName.Contains(searchString));
                //    }
                //    display = "1";
                //    ViewBag.display = display;
                //    ViewBag.searchString = searchString;
                //    ViewBag.Name = Name;
                    
                //    ModelState.Clear();
                //}






              
                //switch (sortOrder)
                //{


                //    case "projectname_desc":
                //        ProjectDetails = ProjectDetails.OrderByDescending(s => s.projectName);
                //        break;
                //    case "date":
                //        ProjectDetails = ProjectDetails.OrderBy(s => s.startDate);
                //        break;
                //    case "date_desc":
                //        ProjectDetails = ProjectDetails.OrderByDescending(s => s.startDate);
                //        break;
                //    case "Enddate_desc":
                //        ProjectDetails = ProjectDetails.OrderByDescending(s => s.endDate);
                //        break;
                //    case "EndDate":
                //        ProjectDetails = ProjectDetails.OrderBy(s => s.endDate);
                //        break;
                //    case "vendorname_desc":
                //        ProjectDetails = ProjectDetails.OrderByDescending(s => s.vendorName);
                //        break;
                //    case "vendorname":
                //        ProjectDetails = ProjectDetails.OrderBy(s => s.vendorName);
                //        break;
                //    case "projectCode":
                //        ProjectDetails = ProjectDetails.OrderBy(s => s.poNumber);
                //        break;
                //    case "projectCode_desc":
                //        ProjectDetails = ProjectDetails.OrderByDescending(s => s.poNumber);
                //        break;
                //    case "duname":
                //        ProjectDetails = ProjectDetails.OrderBy(s => s.duName);
                //        break;
                //    case "duname_desc":
                //        ProjectDetails = ProjectDetails.OrderByDescending(s => s.duName);
                //        break;
                //    case "servicename":
                //        ProjectDetails = ProjectDetails.OrderBy(s => s.type);
                //        break;
                //    case "servicename_desc":
                //        ProjectDetails = ProjectDetails.OrderByDescending(s => s.type);
                //        break;
                //    case "clientName":
                //        ProjectDetails = ProjectDetails.OrderBy(s => s.clientName);
                //        break;
                //    case "clientName_desc":
                //        ProjectDetails = ProjectDetails.OrderByDescending(s => s.clientName);
                //        break;



                //    default:
                //        ProjectDetails = ProjectDetails.OrderBy(s => s.projectName);
                //        break;
                //}


                // var vmsentity = new List<VMSEntity>();
                foreach (var t in ProjectDetails)
                {

                    vmsentity.Add(new Models.VMSEntity()
                    {
                        vendorId = t.vendorId,
                        vendorName = t.vendorName,
                        categoryName = t.categoryName,
                        startDate = t.startDate,
                        endDate = t.endDate,
                      
                     serviceStatus = t.serviceStatus,
                        typeName = t.typeName,
                        poNumber = t.poNumber,
                        serviceCode = t.serviceCode,
                        userDepartmentName = t.userDepartmentName
                    });
                }
                ViewBag.count = count;
                return View(vmsentity);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            return View(vmsentity);

        }

    }



    }
