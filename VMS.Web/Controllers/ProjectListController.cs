﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VMS.DAL.Infrastructure;
using VMS.Entities;

namespace VMS.Web.Controllers
{
    public class ProjectListController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
        // GET: ProjectList
        public ActionResult ProjectList(string id)
        {
           
            var service_details = new Service();
            var category_name = new ServiceCategory();
            var service_type = new ServiceType();
            var user_data = new User();
            var vendor = new Vendor();
            var serviceUsers = new ServiceUser();
            var department_name = new ServiceUserDepartment();
            var projectlistModel = new List<VMS.Models.VMSEntity>();
            var service_modelClass = unitOfWork.DepartmentRepository.GetServiceDetails(service_details, id);
             var category_modelClass = unitOfWork.DepartmentRepository.Get(category_name,service_modelClass[0].serviceCategoryId);
            var department_modelClass = unitOfWork.DepartmentRepository.Get(department_name, service_modelClass[0].userDepartmentId);
            var serviceType_modelClass = unitOfWork.DepartmentRepository.Get(service_type, 0);
            var service_user = unitOfWork.DepartmentRepository.Get(serviceUsers, id);
            var username_data = unitOfWork.DepartmentRepository.Get(user_data, null,service_user[0].firstLevelApprover );
           var  username_data_2 = unitOfWork.DepartmentRepository.Get(user_data, null,service_user[0].secondLevelApprover );
            var  username_data_3 = unitOfWork.DepartmentRepository.Get(user_data, null,service_user[0].thirdLevelApprover );
            var vendor_name  = unitOfWork.DepartmentRepository.Get(vendor, service_modelClass[0].vendorId);

            var podetails = unitOfWork.DepartmentRepository.Getdatails_projectList(id);
            projectlistModel.Add(new VMS.Models.VMSEntity()
                {

                serviceCode = id,
                userDepartmentName = department_modelClass[0].userDepartmentName,
                endDate = service_modelClass[0].endDate,
                serviceValue = service_modelClass[0].serviceValue,
                poNumber = service_modelClass[0].poNumber,
                startDate = service_modelClass[0].startDate,
                serviceStatus = service_modelClass[0].serviceStatus,
               vendorId = vendor_name[0].vendorId,
              typeName = serviceType_modelClass[0].typeName,
                categoryName = category_modelClass[0].categoryName,
                approver1 = username_data[0].userName,
                approver2 = username_data_2[0].userName,
                approver3 = username_data_3[0].userName,
                vendorName = vendor_name[0].vendorName


            });
           // projectlistModel = projectlistModel.Concat(podetails).ToList();
            return View(projectlistModel);
        }
    }
}