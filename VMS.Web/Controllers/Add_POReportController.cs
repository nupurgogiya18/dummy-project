﻿using AutoMapper;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using VMS.BLL;
using VMS.DAL.Infrastructure;
using VMS.Entities;
using VMS.Models;
namespace VMS.Web.Controllers
{
    public class Add_POReportController : Controller 
    {
        private UnitOfWork unitOfWork = new UnitOfWork();
         //IDocumentGenerator documentGenerator = new IDocumentGenerator;
       
       

        
      // private IDocumentGenerator documentGenerator;
        // GET: Add_POReport
        public ActionResult Create(string service_code, string vendor_id)
        {
            var context = new ServiceVMSEntities();
            var service_details = new Service();
            var category_name = new ServiceCategory();
            var department_name = new ServiceUserDepartment();
            //var paymentTerms = new PaymentTerm();
            //var TaxesTerms = new Tax();
            var office_address = new OfficeAddress();

            var service_modelClass = unitOfWork.DepartmentRepository.GetServiceDetails(service_details, service_code);
            var category_modelClass = unitOfWork.DepartmentRepository.Get(category_name,service_modelClass[0].serviceCategoryId);
            var department_modelClass = unitOfWork.DepartmentRepository.Get(department_name, service_modelClass[0].userDepartmentId);
            //var payment_terms = unitOfWork.DepartmentRepository.Get(paymentTerms);
            //var taxes_terms = unitOfWork.DepartmentRepository.Get(TaxesTerms);
            ViewBag.officeAddress = unitOfWork.DepartmentRepository.Get(office_address,0);

            ViewBag.POReportCode = unitOfWork.DepartmentRepository.auto_generatedCode();
            ViewBag.departmentName = department_modelClass[0].userDepartmentName;
            ViewBag.categoryName = category_modelClass[0].categoryName;
            ViewBag.serviceCode = service_code;
            ViewBag.vendorId = vendor_id;
            ViewBag.ponumber = service_modelClass[0].poNumber;
            ViewBag.serviceValue = service_modelClass[0].serviceValue;

          


            var Payment_list = (from t in context.PaymentTerms

                              select t).ToList();

            if (Payment_list != null)
            {
                ViewBag.modelClass1 = Payment_list.Select(N => new SelectListItem { Text = N.PaymentTerms, Value = N.PaymentTerms });
            }






            var Taxes_list = (from t in context.Taxes

                              select t).ToList();

            if(Taxes_list != null)
            {
                ViewBag.modelClass2 = Taxes_list.Select(N => new SelectListItem { Text = N.TaxesTerms, Value = N.TaxesTerms });
            }
           
           
           
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(CKEditor model, HttpPostedFileBase file)
        {
            var file_Value = false;
            if (model.count == 1)
            {
                file_Value = unitOfWork.DepartmentRepository.generatedPO(model);

            }
            else
            {
                file_Value = unitOfWork.DepartmentRepository.filename(file, model.poReportName, model.poReportId);
            }
            var poReort = Mapper.Map<CKEditor, POReport>(model);
            var service = Mapper.Map<CKEditor, Service>(model);

            // file_Value = unitOfWork.DepartmentRepository.filename(file, model.poReportName);

            if (file_Value == true)
            {
            unitOfWork.DepartmentRepository.Insert(poReort);
                    unitOfWork.Save();
                   
                    unitOfWork.DepartmentRepository.service_update(service, model.serviceCode, float.Parse(model.poReportValue));
                    unitOfWork.Save();

                }

            
            return RedirectToAction("ProjectList","ProjectList",new {id = model.serviceCode });
        }
       

        [HttpGet]
        //ajax
        public string GetFileType(string filename)
        {
            //string fileExtn = Path.GetExtension(filename);

            // string mimetype = filename.ContentType;
            int count = 0;
            string contentType = MimeMapping.GetMimeMapping(filename);
            if (contentType == "application/pdf" || contentType == "text/plain" || contentType == "application/msword" || contentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            {
                count = 1;
            }
            else
            {

                count = 0;
            }
            var sprintJson = "{\"count\":" + count + "}";
            return sprintJson;
        }

        public ActionResult Index(string items, string poId, string poname)
        {
            string filePath = null;
            try
            {

                string fileExtn = Path.GetExtension(items);

                filePath = @"C:\VMSDocs\SOWs\" +poname +"_" + poId + "\\" + items;
                Response.AddHeader("Content-Disposition", "inline; filename=" + items);

                

                byte[] FileBytes = System.IO.File.ReadAllBytes(filePath);
                return File(FileBytes, "application/unknown");
            }
            catch (Exception e)
            {
               
            }
            return File(filePath, "application/unknown");
           
        }




        [HttpGet]
        public ActionResult Download(string items, string poId, string poname)
        {
            try
            {
                string FileVirtualPath = @"C:\VMSDocs\SOWs\"  +poname + "_" + poId+ "/" + items;
                return File(FileVirtualPath, "application/force-download", Path.GetFileName(FileVirtualPath));
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception" + e);
            }
            return View();
        }
    }
}
    
