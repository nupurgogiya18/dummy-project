﻿using System;
using System.Web.Mvc;
using VMS.BLL;
using VMS.Models;

namespace VMS.Web.CustomFilters
{
    public class CustomExceptionHandlerFilter : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            ExceptionModel exceptionDetails = new ExceptionModel()
            {
                ExceptionMessage = filterContext.Exception.Message,
                ExceptionStackTrack = filterContext.Exception.StackTrace,
                ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                ActionName = filterContext.RouteData.Values["action"].ToString(),
                ExceptionLogTime = DateTime.Now
            };
            IExceptionLogger exceptionLogger = BLLObjectCreator.CreateLogger(ExceptionLoggerType.Database);
            exceptionLogger.LogException(exceptionDetails);
            filterContext.ExceptionHandled = true;
            filterContext.Result = new ViewResult()
            {
                ViewName = "Error"
            };
        }
    }
}