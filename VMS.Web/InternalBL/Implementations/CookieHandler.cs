﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using VMS.Entities;
using VMS.Models;

namespace VMS.Web.InternalBL.Implementations
{
    public class CookieHandler 
    {
      

        internal static object SetUserCookie(HttpCookieCollection context1, User _userTableDto)
        {
            ServiceVMSEntities context = new ServiceVMSEntities();
            string employeeId = string.Empty;
            string designation = string.Empty;
            var employeeDetails = (from t in context.Users
                                   where t.userEmail == _userTableDto.userEmail
                                   select new { t.employeeId, t.designation });


            foreach (var p in employeeDetails)
            {


                employeeId = p.employeeId;
                    designation = p.designation;


               }
                StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("FirstName:" + _userTableDto.userEmail);

            StringBuilder Role = new StringBuilder();
            Role.Append("" + designation);

            StringBuilder empid = new StringBuilder();
            empid.Append("" + employeeId);




            string userCookie = stringBuilder.ToString();
            string emp = empid.ToString();
            HttpCookie httpCookie1 = new HttpCookie("employeeId");

            httpCookie1.Value = emp;
           // @ViewBag.value = httpCookie1.Value;

            HttpCookie httpCookie3 = new HttpCookie("role");

            httpCookie3.Value = Role.ToString();
           // @ViewBag.value = httpCookie3.Value;



            StringBuilder vid = new StringBuilder();
           // vid.Append("" + ViewBag.vendorId);

            string VendorCookie = stringBuilder.ToString();
            string vendor = vid.ToString();
            HttpCookie httpCookie2 = new HttpCookie("VendorId");

            httpCookie2.Value = vendor;
           // @ViewBag.value = httpCookie2.Value;






            HttpCookie httpCookie = new HttpCookie("vms");
            httpCookie.Value = userCookie;
            httpCookie.Expires = DateTime.Now.AddYears(10);

            context1.Add(httpCookie1);
            context1.Add(httpCookie3);
           
            return httpCookie1.Value;
        }
    }

   
}