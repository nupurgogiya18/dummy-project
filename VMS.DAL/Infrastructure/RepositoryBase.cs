﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using VMS.Entities;
using VMS.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using System.Text.RegularExpressions;

namespace VMS.DAL
{
    public abstract class RepositoryBase<TEntity> where TEntity : class
    {
        internal ServiceVMSEntities context;
        internal DbSet<TEntity> dbSet;

        // internal User<userentity> _myclass;
        // protected MyClass _myclass;

        public RepositoryBase(ServiceVMSEntities context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
            // this.context = context;
            //_myclass = new MyClass();
        }

        public List<VMSEntity> GetServiceDetails(Service service_details, string service_code)
        {
            var service_modelClass = new List<VMSEntity>();
            var Service_List = (from t in context.Services
                                where t.serviceCode == service_code
                                select new { t.poNumber, t.userDepartmentId, t.serviceCategoryId,
                                    t.serviceValue, t.serviceTypeId, t.startDate, t.endDate, t.vendorId }
                                             );

            foreach (var t in Service_List)
            {

                service_modelClass.Add(new VMSEntity()
                {
                    poNumber = t.poNumber,
                    userDepartmentId = t.userDepartmentId,
                    serviceCategoryId = t.serviceCategoryId,
                    serviceValue = t.serviceValue,
                    serviceTypeId = t.serviceTypeId,
                    endDate = t.endDate,
                    startDate = t.startDate,
                    vendorId = t.vendorId,

                });
            }
            return service_modelClass;
        }

        public List<VMSEntity> Get(Vendor vendor, string vendorId)
        {
            var modelClass = new List<VMSEntity>();
            if (vendorId == null)
            {
                

                var Vendor_List = (from t in context.Vendors
                                   select new { t.vendorId, t.vendorName }
                                                 );

                foreach (var t in Vendor_List)
                {

                    modelClass.Add(new VMSEntity()
                    {
                        vendorId = t.vendorId,
                        vendorName = t.vendorName
                    });
                }
            }
            else if(vendorId !=null)
            {
                var Vendor_List = (from t in context.Vendors
                                   where t.vendorId == vendorId
                                   select new {t.vendorName, t.vendorId }
                                            );

                foreach (var t in Vendor_List)
                {

                    modelClass.Add(new VMSEntity()
                    {
                        vendorId = t.vendorId,
                        vendorName = t.vendorName
                    });
                }
            }
           
           
            return modelClass;
        }

        public void service_update(Service service, string id, float poReporlValue)
        {
            float finalserviceVaue = poReporlValue + float.Parse(service.serviceValue);
            var service_update = (from t in context.Services
                                  where t.serviceCode == id
                                  select t).SingleOrDefault();
            service_update.serviceValue = finalserviceVaue.ToString();

        }

        public void Insert(POReport poReort)
        {
            context.POReports.Add(poReort);
        }

        public List<VMSEntity> Get(User approver1, string designation, string employeeid)
        {
            var Approver_1 = new List<VMSEntity>();
            if (employeeid == null)
            {
                
                var Approver1_List = (from t in context.Users
                                      where t.designation == designation
                                      select new { t.employeeId, t.userName }
                                                 );

                foreach (var t in Approver1_List)
                {

                    Approver_1.Add(new VMSEntity()
                    {
                        employeeId = t.employeeId,
                        userName = t.userName
                    });
                }
            }
            else if(designation == null)
            {
              
                var Approver1_List = (from t in context.Users
                                      where t.employeeId == employeeid
                                      select new {t.userName }
                                                 );

                foreach (var t in Approver1_List)
                {

                    Approver_1.Add(new VMSEntity()
                    {
                       
                        userName = t.userName
                    });
                }
            }
            
            return Approver_1;
        }



        public List<VMSEntity> Get(User approver1, string approver,int count)
        {
            var Approver_1 = new List<VMSEntity>();
            

                var Approver1_List = (from t in context.Users
                                      where t.userDepartmentId == count && t.designation == approver
                                      select new { t.employeeId, t.userName }
                                                 );

                foreach (var t in Approver1_List)
                {

                    Approver_1.Add(new VMSEntity()
                    {
                        employeeId = t.employeeId,
                        userName = t.userName
                    });
                }
           
        


            return Approver_1;
        }

        public List<VMSEntity> Get(PaymentTerm payment_term)
        {
            var payment = new List<VMSEntity>();
           
                var Payment_list = (from t in context.PaymentTerms
                                    
                                      select new { t.PaymentTermsId, t.PaymentTerms }
                                                 );

                foreach (var t in Payment_list)
                {

                    payment.Add(new VMSEntity()
                    {
                        PaymentTermsId = t.PaymentTermsId,
                        PaymentTerms = t.PaymentTerms
                    });
                }
          

            return payment;
        }

        public List<VMSEntity> Get(OfficeAddress office_address, int id)
        {
            var Off_Add = new List<VMSEntity>();
            if (id == 0)
            {
               

                var Address_list = (from t in context.OfficeAddresses

                                    select new { t.addressId, t.locationName }
                                                 );

                foreach (var t in Address_list)
                {

                    Off_Add.Add(new VMSEntity()
                    {
                        addressId = t.addressId,
                        locationName = t.locationName
                    });
                }
            }
            else
            {




                var Address_list = (from t in context.OfficeAddresses
                                    where t.addressId == id
                                    select new { t.locationAddress }
                                                 );

                foreach (var t in Address_list)
                {

                    Off_Add.Add(new VMSEntity()
                    {
                      
                        locationAddress = t.locationAddress
                    });
                }
            }

            return Off_Add;
        }

        













        public List<VMSEntity> Get(ServiceUser serviceuser, string id)
        {
            var username = new List<VMSEntity>();
            var Approvers_List = (from t in context.ServiceUsers
                                  where t.serviceCode == id
                                  select new { t.firstLevelApprover, t.secondLevelApprover,t.thirdLevelApprover }
                                             );

            foreach (var t in Approvers_List)
            {

                username.Add(new VMSEntity()
                {
                    firstLevelApprover = t.firstLevelApprover,
                    secondLevelApprover = t.secondLevelApprover,
                    thirdLevelApprover = t.thirdLevelApprover
                });
            }
            return username;
        }






        public List<VMSEntity> Get(ServiceUserDepartment userDepartmenttype, int userDepartmentId)
        {
            var service_user = new List<VMSEntity>();
            if (userDepartmentId == 0)
            {
                var department_List = (from t in context.ServiceUserDepartments
                                       select new { t.userDepartmentId, t.userDepartmentName }
                                             );

                foreach (var t in department_List)
                {

                    service_user.Add(new VMSEntity()
                    {
                        userDepartmentId = t.userDepartmentId,
                        userDepartmentName = t.userDepartmentName
                    });
                }
            }
            else if (userDepartmentId > 0)
            {
                var department_name = (from department in context.ServiceUserDepartments
                                       where department.userDepartmentId == userDepartmentId
                                       select new { department.userDepartmentName });
                foreach (var department in department_name)
                {

                    service_user.Add(new VMSEntity()
                    {

                        userDepartmentName = department.userDepartmentName
                    });
                }
            }


            return service_user;
        }

        public List<VMSEntity> Get(ServiceType service_type, int serviceTypeId)
        {
            var Service_Type = new List<VMSEntity>();
            if (serviceTypeId == 0)
            {
                var Type_List = (from t in context.ServiceTypes
                                 select new { t.serviceTypeId, t.typeName }
                                             );

                foreach (var t in Type_List)
                {

                    Service_Type.Add(new VMSEntity()
                    {
                        serviceTypeId = t.serviceTypeId,
                        typeName = t.typeName
                    });
                }
            }
            else if (serviceTypeId > 0)
            {
                var Type_List = (from t in context.ServiceTypes
                                 where t.serviceTypeId == serviceTypeId
                                 select new {t.typeName }
                                                            );

                foreach (var t in Type_List)
                {

                    Service_Type.Add(new VMSEntity()
                    {
                        typeName = t.typeName
                       
                    });
                }
            }
            return Service_Type;
        }

        public List<VMSEntity> Get(ServiceCategory service_category, int serviceCategoryId)
        {
            var Service_Category = new List<VMSEntity>();
            if (serviceCategoryId == 0)
            {

                var Category_List = (from t in context.ServiceCategories
                                     select new { t.serviceCategoryId, t.categoryName }
                                                 );

                foreach (var t in Category_List)
                {

                    Service_Category.Add(new VMSEntity()
                    {
                        serviceCategoryId = t.serviceCategoryId,
                        categoryName = t.categoryName
                    });
                }
            }
            else if (serviceCategoryId > 0)
            {
                var categoryName = (from category in context.ServiceCategories
                                    where category.serviceCategoryId == serviceCategoryId
                                    select new { category.categoryName });
                foreach (var category in categoryName)
                {

                    Service_Category.Add(new VMSEntity()
                    {

                        categoryName = category.categoryName
                    });
                }

            }

            return Service_Category;
        }

        public void Insert(ServiceVendor service_vendor)
        {
            context.ServiceVendors.Add(service_vendor);
        }

        public void Insert(ServiceUser service_user)
        {
            context.ServiceUsers.Add(service_user);
        }

        public void Insert(Service service)
        {

            context.Services.Add(service);
        }

        public void Insert(ServiceType serviceType)
        {
            context.ServiceTypes.Add(serviceType);
        }













        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }




        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }






        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public string auto_generatedCode()
        {

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            var stringChars = new char[4];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            string milliseconds = DateTime.Now.ToString();

            string[] value = milliseconds.Split(' ');
            string final_datevalue = value[0].Replace("/", "");
            var time_length = value[1].Length;
            string time_substring = null;
            string time_stampValue = null;
            if (time_length == 7)
            {
                time_substring = value[1].Substring(0, 4);
            }
            else
            {
                time_substring = value[1].Substring(0, 5);
            }
            string final_timevalue = time_substring.Replace(":", "");
            time_stampValue = finalString + final_datevalue + final_timevalue + DateTime.Now.Millisecond;
            int time_stampValue_length = time_stampValue.Length;
            if (time_stampValue_length > 16)
            {
                time_stampValue = time_stampValue.Substring(0, 16);
            }

            return time_stampValue;
        }

        public bool filename(HttpPostedFileBase file, string poName, string id)
        {
           
            bool Value = false;
            
            if (file.ContentLength > 0)
            {
                Value = true;
                string fileExtn = Path.GetExtension(file.FileName);
                string mimetype = file.ContentType;

                string newFileName = id; // Without Extn.
                                         // string path1 = Path.Combine(@"C:\uploads\sow\", newFileName);
                var path = Path.Combine(@"C:\VMSDocs\SOWs\"+ poName + "_" + id , newFileName);
                 var filename = file.FileName;
                if (!Directory.Exists(path))
                {

                    Directory.CreateDirectory(path);
                }
                string StrFilePathName = path + fileExtn;
                file.SaveAs(StrFilePathName);
                var filelength = file.ContentLength;
                var fileName = StrFilePathName;
            }

            return Value;
        }
        struct MyStruct
        {

            public string Subject;
          //  public string QuotationDetails;
            public string PaymentMilestones;
        }
        public bool generatedPO(CKEditor model)
        {
            string Taxdetail = null;
            string Paymentdetail = null;
            string billingdetail = null;
            string str1 = null;
            string str2 = null;
            int paymentdetail_Size = 0;

            for (int i = 0; i < model.paymentDetail.Length; i++)
            {

                if (i == 0)
                {
                    Paymentdetail = "<div style=\"padding-left: " + 20 + "\";><ul>";
                    Paymentdetail = Paymentdetail + "<li>  " + model.paymentDetail[i] + "</li>";
                    // Paymentdetail = "<div style=\"position: absolute;\">&emsp;•</div> " + "<div style=\"padding-left: " + 50 +"px" +"\";>" + Paymentdetail + model.paymentDetail[i] + "</div><br/>";


                }
                else if(i == (model.paymentDetail.Length)-1)
                {
                    Paymentdetail = Paymentdetail + "<li>  " + model.paymentDetail[i] + "</li></ul></div>";
                }
                else
                {
                    Paymentdetail = Paymentdetail + "<li>  " + model.paymentDetail[i] + "</li>";
                }

            }
            for (int i = 0; i < model.taxDetail.Length; i++)
            {
                
                
                    if(i == 0)
                    {
                        Taxdetail = "<div style=\"padding-left: " + 20 + "\";><ul><li>  " + Taxdetail + model.taxDetail[i] + "</li>";
                }
                else if (i == (model.taxDetail.Length) - 1)
                {
                    Taxdetail = Taxdetail + "<li>  " + model.taxDetail[i] + "</li></ul></div>";
                }
                else
                    {
                        Taxdetail =  Taxdetail + "<li>  " + model.taxDetail[i] + "</li>";
                    }
              
            }

            var office_Address = new OfficeAddress();
            
            for (int i = 0; i < model.BillingUnit.Length; i++)
            {


                if (model.BillingUnit[i].Contains("HSPL"))
                {
                    var billingDAta = Get(office_Address, model.hsplBillingAddressId);
                    var shippingDataDAta = Get(office_Address, model.hsplShippingAddressId);

                    billingdetail = billingdetail + "<tr><td>" + model.BillingUnit[i] + "</td><td>" + billingDAta[0].locationAddress +
                        "</td><td>" + shippingDataDAta[0].locationAddress + "</td><td>GST</td><td>" + model.HSPLpercent + "</td></tr>";
                }
                if (model.BillingUnit[i].Contains("HILPL"))
                {
                    var billingDAta = Get(office_Address, model.hilplBillingAddressId);
                    var shippingDataDAta = Get(office_Address, model.hilplShippingAddressId);
                    billingdetail = billingdetail + "<tr><td>" + model.BillingUnit[i] + "</td><td>" + billingDAta[0].locationAddress +
                        "</td><td>" + shippingDataDAta[0].locationAddress + "</td><td>GST</td><td>" + model.HILpercent + "</td></tr>";
                }
                if (model.BillingUnit[i].Contains("HTV"))
                {
                    var billingDAta = Get(office_Address, model.htvBillingAddressId);
                    var shippingDataDAta = Get(office_Address, model.htvShippingAddressId);
                    billingdetail = billingdetail + "<tr><td>" + model.BillingUnit[i] + "</td><td>" + billingDAta[0].locationAddress +
                        "</td><td>" + shippingDataDAta[0].locationAddress + "</td><td>GST</td><td>" + model.HTVpercent + "</td></tr>";
                }
                if (model.BillingUnit[i].Contains("HKP"))
                {
                    var billingDAta = Get(office_Address, model.hkpplBillingAddressId);
                    var shippingDataDAta = Get(office_Address, model.hkpplShippingAddressId);
                    billingdetail = billingdetail + "<tr><td>" + model.BillingUnit[i] + "</td><td>" + billingDAta[0].locationAddress +
                        "</td><td>" + shippingDataDAta[0].locationAddress + "</td><td>GST</td><td>" + model.HKPpercent + "</td></tr>";
                }

            }
            string details = null;
            string billingunit = null;

            //if(model.BillingUnit == "HSPL")
            //{
            //    billingunit = "Harbinger System Pvt. Ltd.";
            //}
            //if (model.BillingUnit == "HILPL")
            //{
            //    billingunit = "Harbinger Interactive Learning Pvt. Ltd.";
            //}
            //if (model.BillingUnit == "HTV")
            //{
            //    billingunit = "Harbinger TechVentures Pvt. Ltd.";
            //}
            //if (model.BillingUnit == "HKP")
            //{
            //    billingunit = "Harbinger Knowledge Pvt. Ltd.";
            //}
           
            

            Regex rgx = new Regex("<p>|</p>");
            var tag = "(</p>)";
            string subj = rgx.Replace(model.Subject, "", 1);
            bool tablecheck = model.Details.Contains("</table>");
            if (tablecheck == true)
            {
                string[] detai = null;
                detai = Regex.Split(model.Details, tag);
                int n = detai.Length;
                Console.Write(n);
                //  foreach(var i in detai.Length)
                for (int i = 0; i < n; i++)
                {
                    if (detai[i].Contains("<table"))
                    {
                        details = details + "<br/>" + detai[i];
                    }
                    else
                    {
                        details = details + detai[i];
                    }

                }
                details = "<font face=\"Verdana\"size = 2>" + details + "</font>";

            }
            else
            {
                details = "<font face=\"Verdana\"size = 2>" + model.Details + "</font>";
            }

           string quatationdate = (model.QuotationDate).ToShortDateString();
            string delivery = null;
            string finalcontractdate = null;
            string currentdate = (model.PoDate).ToShortDateString();
            if(model.deliverytextbox == null)
            {
                 //from_Date = (model.ContractFromDate).ToShortDateString();
               //  to_Date = (model.ContractToDate).ToShortDateString();
                finalcontractdate = (model.ContractFromDate).ToShortDateString() + " to " + (model.ContractToDate).ToShortDateString();
                delivery = "NA";
            }
            else
            {
                finalcontractdate = "NA";
                delivery = model.deliverytextbox;
            }
            
            string vendorname = null;
            string vendorEmail = null;
            string poc = null;
            string address = null;
            string thirdlevel_approver = null;
            var vendors = new List<VMSEntity>();
            var vendor_details = (from t in context.Vendors
                                  
                                  where t.vendorId == model.vendorId
                                  select new { t.vendorName, t.poc, t.vendorEmail, t.vendorAddress });
            foreach (var u in vendor_details)
            {
                vendorname = u.vendorName;
                vendorEmail = u.vendorEmail;
                poc = u.poc;
                address = u.vendorAddress;
            }

            var service_user = new ServiceUser();
            var service = new User();
            var approver3 = Get(service_user,  model.serviceCode);
            var approver3_name = Get(service, null, approver3[0].thirdLevelApprover);
            
            MyStruct myStruct;

            myStruct.Subject = model.Subject;
            //myStruct.QuotationDetails = model.QuotationDetails;
            myStruct.PaymentMilestones = model.Details;

            var OutputPath = @"C:\VMSDocs\SOWs\" + model.poReportName + "_" + model.poReportId;
            if (!Directory.Exists(OutputPath))
            {
                Directory.CreateDirectory(OutputPath);
            }

            else
            {
                string[] oldfilename = Directory.GetFiles(@"C:\VMSDocs\SOWs\" + model.poReportName + "_" + model.poReportId);
                List<string> items = new List<string>();
                foreach (string file1 in oldfilename)
                {
                    System.IO.File.Delete(file1);
                }
            }






            iTextSharp.text.Document document = new iTextSharp.text.Document();
            iTextSharp.text.Font boldFont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.Font.BOLD);
            iTextSharp.text.Font simple = FontFactory.GetFont("Verdana", 10);
            iTextSharp.text.Font boldItalicFont = FontFactory.GetFont("Verdana", 10, iTextSharp.text.Font.BOLDITALIC);


            // document.add(p);
            //String content = "The quick brown fox jumps over the lazy dog";
            //Paragraph para = new Paragraph();
            Chunk glue = new Chunk(new VerticalPositionMark());
            Phrase ph1 = new Phrase();
            //ph1.Add(new Chunk(Environment.NewLine));

            Paragraph main = new Paragraph();
            ph1.Add(new Chunk(model.poNumber, boldFont)); // Here I add projectname as a chunk into Phrase.    
            ph1.Add(glue); // Here I add special chunk to the same phrase.    
            ph1.Add(new Chunk(currentdate, boldFont)); // Here I add date as a chunk into same phrase.    
            main.Add(ph1);

            string TO = "<br/>" +
                        "<font face=\"Verdana\" size=2><h5><b>To,</b></h5><\n>" +

                        "<h5><b>" + vendorname + "</b></h5><\n>" +
                        "<h5>" + address + "</h5><\n>" +
                        "<h5>" + vendorEmail + " </h5>" + "<br/>" +
             "<b>Kind Attn. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ":</b>" + poc + "<br/>" +
             "<b>Subject &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ":</b>" + subj +
             "<b>Reference &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + ":Quotation Number: </b>" + model.QuotationNumber + " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Dated:</b> " + quatationdate + "</font><br/><br/>";

            string salutation = "<font face=\"Verdana\" size=2><h5>Dear Sir/Ma’am,</h5><\n>" +
                "With reference to your quotation and subsequent discussion we would like to place a work order as follows.</font><br/>";


            string termsAndConditions = "<font face=\"Verdana\" size=2><br/><br/><b><u>Terms " + "And" + " Conditions :</u></b><br/><br/>" +
                "<ol><li><b>Payment Terms - </b>" + Paymentdetail +"</li>"+
                "<li><b>Taxes -</b>" + 
                Taxdetail + "</li>" +
                "<li><b>Contract Period - </b>" + finalcontractdate + "</li>" +
                "<li><b>Delivery –</b>" + delivery + "</li>" +
                "<li><b>Statutory Compliance - </b>Vendor will be responsible for P.F .P.T., ESIC. and any other statutory compliance if applicable for their staff working at our office for our scheduled work and will provide us the documents if required at any given point of time.</li>" +
                "<li><b>Acceptance -</b>Decision of Harbinger about acceptance of the system, goods or services ordered under this PO (“Material”) shall be final and binding on the <b>" + vendorname +
                "</b>. Mere acknowledgement of receipt of services shall not constitute as acceptance. The acceptance shall be subject to the compliance by the <b>" + vendorname + "</b> of the design and other specifications" +
                " of Harbinger and the Material supplied should be it should be new, merchantable quality and fit for their intended purpose. Harbinger reserves a right to reject any portion of the Materials ordered under this Purchase Order at any time even after the acceptance if they are found defective or not in conformity with the specification or not fit for their intended purpose without having any liability to pay for the such portion, if so desired by the Harbinger.</li>" +

                "<li><b>Quality " + "& " + " Service levels -</b>The Material shall comply with the specifications and approved sample (as the case may be) in terms of quality, specifications, design and service levels and shall be no less than industry standard and shall be free of defect or deficiency including in design, system functioning, update etc. The Materials shall be exactly as per the ordered quantity and as per the requirement of Harbinger.</li>" +

                "<li><b>Cancellation of Purchase Order - </b>Harbinger shall have the right to revise the PO or cancel the PO in the event it does not meet the requirements with regard to quality and service of Harbinger</li>" +
                "<li><b>Indemnity -" + vendorname + "</b>  shall indemnify Harbinger, its employees, officers, agents and representatives from and against all claims for damages, or other losses due to the use of goods, services, equipment, devices or processes used in the delivery of goods or services.</li>" +
                "<li><b>Confidential Information - </b>Each party will keep in strict confidence all technical or commercial know-how, specifications, inventions, processes or initiatives which are of a confidential nature and have been disclosed to that party or its agents and any other confidential information concerning the disclosing party's business or its products which the receiving party may obtain and the receiving party will restrict disclosure of such confidential material to such of its employees, agents or subcontractors as need to know the same for the purpose of discharging the receiving party's obligations to the disclosing party and will ensure that such employees, agents or subcontractors are subject to like obligations of confidentiality as bind the receiving party. No personal data shall be provided to the" + vendorname + " under this purchase order. </li>" +
                "<li><b>Assignment - </b>The<b> " + vendorname + " </b>will not be entitled to assign this purchase order or any part of it without the prior written consent of Harbinger.</li>" +
                "<li><b>Compliance with Laws - " + vendorname + "</b> represents and warrants that it complies with all applicable laws of India.</li>";


           

            string biliigDetails = "<li><b>Billing Address: </b><table border = 1 style=\"text-align:center\"><tr><b><th>Billing Unit</th>" +
                "<th>Billing Address</th><th>Shipping Address</th><th>GST</th><th>Percentage</th></b></tr> " + billingdetail +"</table></li><br/><br/>";

            string lastpoint=  "<br/><br/><li>Your invoice must have the <b>PO number and GST numbers (wherever applicable).</b></li></ol>"+
                "<br/><br/>Thanking you,</font>";

            string final = termsAndConditions + biliigDetails + lastpoint;

            //string nn = "<ol><li>ghfdhfsh</li><ol><li>shgdf</li></ol></ol>";

            string spacing = "<br/><br/>";

           

            Chunk glue1 = new Chunk(new VerticalPositionMark());
            Paragraph p = new Paragraph();
            p.Add(new Chunk("For",simple));
            p.Add(new Chunk(" " + billingunit, boldFont));
            p.Add(new Chunk(glue1));
            p.Add(new Chunk("                                                 For ", simple));
            p.Add(new Chunk(billingunit +"                      ", boldFont));

           // string space = "<br/><br/>";



            Paragraph p1 = new Paragraph();
            p1.Add(new Chunk(approver3_name[0].userName + "" , boldFont));
            p1.Add(new Chunk(glue1));
            p1.Add(new Chunk("                                                                                                           Tejashri Barve", boldFont));


            Paragraph p2 = new Paragraph();
            p2.Add(new Chunk("General Manager- Facilities & Admin", simple));
            p2.Add(new Chunk(glue1));
            p2.Add(new Chunk("                                                                   GM - Finance & Accounts", simple));

            //

            // string parsedstring = PDF.ToString();
            PdfWriter.GetInstance(document, new FileStream(OutputPath + "\\" + model.poReportName + ".pdf", FileMode.Create));


            //string footer = "<font face=\"verdana\" size=2><br/> © Harbinger Interactive Learning Pvt. Ltd </font>";

            document.Open();

            iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document);
            iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();

            
            document.Add(main);
            hw.Parse(new StringReader(TO));
             hw.Parse(new StringReader(salutation));
             hw.Parse(new StringReader(details));
            hw.Parse(new StringReader(final));
            
            hw.Parse(new StringReader(spacing));
            document.Add(p);
            hw.Parse(new StringReader(spacing));
        
            
            document.Add(p1);
            document.Add(p2);
            document.Close();
            return true;
        }

        //public List<VMSEntity> GetPoDetails(POReport poreport, string id)
        //{
        //    string y = null;
        //    var vendor_podetails = (from sc in context.POReports
        //                            where sc.serviceCode == id
        //                            select new { sc.poReportId, sc.poReportValue, sc.poReportStatus, sc.poReportName});
        //    return y;
        //}
            public List<VMSEntity> Getdatails_projectList(string id)
        {
           

            var vendor_podetails =(from sc in context.POReports                               
                                    where sc.serviceCode == id
                                    select new { sc.poReportId,  sc.poReportValue, sc.poReportStatus, sc.poReportName });


            string code4 = null;

            string[] allPO = new string[vendor_podetails.Count()];
            int i = 0;
            var employeeModel = new List<VMS.Models.VMSEntity>();
            float projectionValue = 0;
            foreach (var t in vendor_podetails)
            {

                employeeModel.Add(new Models.VMSEntity()
                {
                    poReportId = t.poReportId,
                    poReportValue = t.poReportValue,
                    poReportStatus = t.poReportStatus,
                    poReportName = t.poReportName
                });
                

                
                allPO[i] = t.poReportId;
                i++;
            }
            //int sprintcount = 0;
            //var sowExists = (from sow in context.Proposals
            //                 where sow.projectCode == decodeProjectcode
            //                 select new { sow.proposalId });

            //foreach (var t in sowExists)
            //{
            //    sprintcount = 1;
            //    ViewBag.sprintcount = sprintcount;
            //}
            var p = (from t in context.POReports
                     join sc in context.Sprints on t.poReportId equals sc.poReportId
                     where allPO.Contains(t.poReportId)
                     select new { sc.sprintId, sc.sprintName, sc.percentOfServiceValue, sc.sprintEndDate, sc.sprintValue, sc.poReportId });
            float invoiceAmount = 0;
            float invoiceAmountFinal = 0;
            string sprintId = null;
            string poreportid = null;
            string sprintPercent = null;
            float decimalPercent;

            string sprintValue = null;
            float decimalValue;
            foreach (var t in p)
            {
                decimalPercent = float.Parse(t.percentOfServiceValue);
                sprintPercent = decimalPercent.ToString("0.00");

                decimalValue = float.Parse(t.sprintValue);
                sprintValue = decimalValue.ToString("0.00");
                employeeModel.Add(new Models.VMSEntity()
                {

                    sprintId = t.sprintId,
                    sprintName = t.sprintName,
                    percentOfProjectValue = sprintPercent,

                    sprintEndDate = t.sprintEndDate,
                    sprintValue = t.sprintValue,
                    poReportId = t.poReportId
                });

                sprintId = t.sprintId;
                poreportid = t.poReportId;
                projectionValue = projectionValue + float.Parse(t.sprintValue);

            }
            //var Invoice = (from t in context.Invoices
            //               where (t.serviceCode == id) && (t.invoiceStatus != "Rejected by DH" && t.invoiceStatus != "Rejected by DM" && t.invoiceStatus != "Rejected by VM" && t.invoiceStatus != "Rejected by FM")
            //               select new { t.invoiceAmount }
            //       );
            //foreach (var t in Invoice)
            //{

            //    employeeModel.Add(new Models.VMSEntity()
            //    {
            //        invoiceAmount = t.invoiceAmount,

            //    });
            //    invoiceAmount = float.Parse(t.invoiceAmount);
            //    invoiceAmountFinal = invoiceAmountFinal + invoiceAmount;
            //}







            //ViewBag.invoiceAmount = invoiceAmountFinal;
            //ViewBag.projectionValue = projectionValue;
         
           





            return employeeModel;

        }
    }
}

