﻿using VMS.Entities;

namespace VMS.DAL
{
    internal class ExceptionLoggerRepository : RepositoryBase<ExceptionLogger>
    {
        public ExceptionLoggerRepository(ServiceVMSEntities context) : base(context)
        {
        }
    }
}
