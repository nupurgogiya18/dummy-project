﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Entities;

namespace VMS.DAL.Repositories
{
    internal class Service : RepositoryBase<Service>
    {

        public Service(ServiceVMSEntities context) : base(context)
        {

        }
    }
}
