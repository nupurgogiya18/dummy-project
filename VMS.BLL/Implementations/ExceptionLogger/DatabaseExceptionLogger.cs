﻿using AutoMapper;
using VMS.DAL.Infrastructure;
using VMS.Entities;
using VMS.Models;

namespace VMS.BLL
{
    internal class DatabaseExceptionLogger : IExceptionLogger
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public void LogException(ExceptionModel exceptionDetails)
        {
            var entity = Mapper.Map<ExceptionModel, ExceptionLogger>(exceptionDetails);

            unitOfWork.ExceptionLoggerRepository.Insert(entity);
            unitOfWork.Save();
            unitOfWork.Dispose();
        }
    }
}
