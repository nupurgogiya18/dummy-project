﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMS.BLL
{
    public static class BLLObjectCreator
    {
        public static IExceptionLogger CreateLogger(ExceptionLoggerType logger = ExceptionLoggerType.Database)
        {
            switch (logger)
            {
                case ExceptionLoggerType.Text:
                    return new TextExceptionLogger();
                case ExceptionLoggerType.Database:
                    return new DatabaseExceptionLogger();
                default:
                    return new TextExceptionLogger();
            }
        }
    }
}
