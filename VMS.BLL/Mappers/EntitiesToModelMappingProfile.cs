﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using VMS.Entities;
using VMS.Models;

namespace VMS.BLL.Mappers
{
    internal class EntitiesToModelMappingProfile : Profile
    {
        public EntitiesToModelMappingProfile()
        {
            CreateMap<ExceptionLogger, ExceptionModel>();
            CreateMap<VMSEntity, ServiceType>();
            CreateMap<VMSEntity, Service>();
            CreateMap<VMSEntity, ServiceUser>();
            CreateMap<VMSEntity, ServiceVendor>();
            CreateMap<CKEditor, POReport>();

            CreateMap<CKEditor, Service>();
        }
    }
}
