﻿using AutoMapper;
using VMS.Entities;
using VMS.Models;

namespace VMS.BLL.Mappers
{
    internal class ModelToEntitiesMappingProfile : Profile
    {
        public ModelToEntitiesMappingProfile()
        {
            CreateMap<ExceptionModel, ExceptionLogger>();
        }
    }
}
