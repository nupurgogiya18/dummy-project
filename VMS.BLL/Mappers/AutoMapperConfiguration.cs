﻿using AutoMapper;

namespace VMS.BLL.Mappers
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<EntitiesToModelMappingProfile>();
                x.AddProfile<ModelToEntitiesMappingProfile>();
            });
        }
    }
}
