﻿using VMS.Models;

namespace VMS.BLL
{
    public interface IExceptionLogger
    {
        void LogException(ExceptionModel exceptionDetails);
    }
}
