﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.Models;

namespace VMS.BLL
{
  public  interface IDocumentGenerator 
    {
        string getdate(string id);
        void generatePDF(CKEditor model);
    }
}
